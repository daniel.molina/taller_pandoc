# Taller de Pandoc

Este es un taller organizado por la [Oficina de Sofrware Libre de la
UGR](https://osl.ugr.es/). 

El objetivo de este taller es presentar el pandoc y markdown como opciones
sencillas para generar documentación y transparencias. 

https://osl.ugr.es/2021/11/03/taller-de-pandoc.

El taller se impartirá el día 10 de
Noviembre de 19:30 a 21:30:

- Presencialmente: en la ETSIIT de la Univerisdad de Granada, en el aula 0.6.
- Virtualmente: en Youtube (enlace por confirmar).

![](pandoc_cartel.png)
